/*1)Часто потібно використати повторення певних дій і для того щоб зменшити кількість коду та написання коду використовують цикли*/
/*2)Якщо ми знаємо точну кількість ітерацій, наприклад вивести чи перебрати числа від 0 до 50 ми може використати цикл for*/
/*Якщо ми не знаємо точно кількість ітерацій, наприклад перевірка введених даних користувачем і повторювати певні дії доки він не введе правильні дані ми може використати цикл while*/
/*Якщо ми хочемо щоб код виконався хочаб один раз, наприклад, якщо ми хочемо попросити користувача ввести дані доти, доки вони не будуть відповідати умові ми може використати цикл do while*/
/*3)Явне приведення типів - це коли ми вказуємо, що потрібно перетворити один тип на інший(явна зміна типу даних)*/
/*Неявне приведення типів - це автоматичне перетворення типу, яке відбувається без явної зміни типу*/
let counter = 0;
let userNumber = prompt("enter your integer");
while (isNaN(userNumber) || Number(userNumber) <= 0 || !Number.isInteger(Number(userNumber))) {
    alert("Please enter valid information!");
    userNumber = prompt("Enter your integer:", userNumber);
}

for (let i = 5; i <= userNumber; i++) {
    if (i % 5 === 0) {
        console.log(i);
        counter++;
    }
}

if (counter === 0) {
    console.log("Sorry, no numbers");
}

let m = prompt("Enter first number:");
let n = prompt("Enter another number:");

while (isNaN(m) || isNaN(n) || Number(m) >= Number(n) || !Number.isInteger(Number(m)) || !Number.isInteger(Number(n))) {
    alert("Please enter valid information!");
    m = prompt("Enter first number:",m);
    n = prompt("Enter another number:",n);
}

for (let i = m; i <= n; i++) {
    let isPrime = true;
    for (let j = 2; j <= i / 2 ; j++) {
        if (i % j === 0) {
            isPrime = false;
            break;
        }
    }
    if (isPrime && i !== 1) {
        console.log(i);
    }
}